import 'package:flutter/material.dart';
import 'package:flutter_inapp_takeover/flutter_inapp_takeover.dart';
import 'package:flutter_inapp_takeover/network/network.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String baseUrl = 'tfdh1lcazl.execute-api.af-south-1.amazonaws.com';
  String userId = '13b20239a29335';
  String inAppId = 'e25afd16-ab51-4d3c-ab7a-d1fb00b99614';

  @override
  void initState() {
    super.initState();

    InAppTakeOverNetworkLayer().init(
      baseUrl: baseUrl,
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('In-App Take Over example app'),
        ),
        body: Center(
          child: Column(
            children: [
              RaisedButton(
                  child: Text('Get TakeOver'),
                  onPressed: () {
                    InAppTakeOverService.getTakeOvers(authId: userId).then(
                        (value) {
                      print(value);
                    }, onError: (e) {
                      print('Error');
                      print(e);
                    });
                  }),
              RaisedButton(
                  child: Text('Mark read'),
                  onPressed: () {
                    InAppTakeOverService.markTakeOverAsRead(inAppId: inAppId)
                        .then((value) {
                      print(value);
                    }, onError: (e) {
                      print('Error');
                      print(e);
                    });
                  })
            ],
          ),
        ),
      ),
    );
  }
}
