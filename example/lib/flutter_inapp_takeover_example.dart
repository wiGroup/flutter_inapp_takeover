import 'dart:async';

import 'package:flutter/services.dart';

class FlutterInappTakeoverExample {
  static const MethodChannel _channel =
      const MethodChannel('flutter_inapp_takeover_example');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }
}
