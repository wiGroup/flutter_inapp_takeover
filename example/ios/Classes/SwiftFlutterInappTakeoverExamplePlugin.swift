import Flutter
import UIKit

public class SwiftFlutterInappTakeoverExamplePlugin: NSObject, FlutterPlugin {
  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "flutter_inapp_takeover_example", binaryMessenger: registrar.messenger())
    let instance = SwiftFlutterInappTakeoverExamplePlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    result("iOS " + UIDevice.current.systemVersion)
  }
}
