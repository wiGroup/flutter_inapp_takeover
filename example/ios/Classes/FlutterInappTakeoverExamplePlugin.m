#import "FlutterInappTakeoverExamplePlugin.h"
#if __has_include(<flutter_inapp_takeover_example/flutter_inapp_takeover_example-Swift.h>)
#import <flutter_inapp_takeover_example/flutter_inapp_takeover_example-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "flutter_inapp_takeover_example-Swift.h"
#endif

@implementation FlutterInappTakeoverExamplePlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftFlutterInappTakeoverExamplePlugin registerWithRegistrar:registrar];
}
@end
