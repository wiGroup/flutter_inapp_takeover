library flutter_inapp_takeover;

import 'package:flutter_abstract_network_payload/network/network.dart';
import 'package:flutter_inapp_takeover/network/network.dart';

import 'models/models.dart';

class InAppTakeOverService {
  static Future getTakeOvers({String authId}) async {
    return InAppTakeOverNetworkLayer()
        .request(HttpMethod.get, InAppTakeOverRouter.getTakeOvers,
            TakeOverPayLoad(userId: authId))
        .then((response) {
      return Future.value(TakeOverResponse.fromJson(response));
    });
  }

  static Future markTakeOverAsRead({String inAppId}) async {
    return InAppTakeOverNetworkLayer()
        .request(HttpMethod.put, InAppTakeOverRouter.markAsRead,
            TakeOverReadPayLoad(inAppId: inAppId))
        .then((response) {
      return Future.value(TakeOverResponse.fromJson(response));
    });
  }
}
