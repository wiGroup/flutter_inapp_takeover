class TakeOverItem {
  String contentfulId;
  String id;

  TakeOverItem({this.contentfulId, this.id});

  factory TakeOverItem.fromJson(Map<String, dynamic> json) {
    return TakeOverItem(contentfulId: json['contentful_id'], id: json['id']);
  }
}
