class ApiResponse {
  final int httpCode;
  final dynamic code;
  final String desc;

  ApiResponse({this.httpCode, this.code, this.desc});

  factory ApiResponse.fromJson(Map<String, dynamic> json) {
    return ApiResponse(
      httpCode: json['httpCode'],
      code: json['code'],
      desc: json['desc'],
    );
  }
}
