import 'package:flutter_abstract_network_payload/network/http_request_algorithm.dart';
import 'package:flutter_inapp_takeover/models/takeover_response.dart';
import 'package:flutter_inapp_takeover/network/inapp_takeover_payload.dart';

class TakeOverReadPayLoad extends InAppTakeOverPayLoad {
  String inAppId;

  TakeOverReadPayLoad({this.inAppId});

  @override
  Map<String, String> get additionalHeaders => null;

  @override
  Map<String, String> get queryParameters => {
        'inapp_id': inAppId,
      };

  @override
  Map<String, dynamic> request() => null;

  @override
  TakeOverResponse response(Map<String, dynamic> json) =>
      TakeOverResponse.fromJson(json);

  @override
  Map<String, String> get pathParameters => null;

  @override
  TakeOverResponse responseWithStatus(
          Map<String, dynamic> json, int statusCode) =>
      null;

  @override
  EHttpRequestAlgorithm get requestAlgorithm => EHttpRequestAlgorithm.replace;
}
