import 'models.dart';

class TakeOverResponse {
  ApiResponse response;
  TakeOverItem takeOverItem;

  TakeOverResponse({this.response, this.takeOverItem});

  factory TakeOverResponse.fromJson(Map<String, dynamic> json) =>
      TakeOverResponse(
        response: json['response'] != null
            ? ApiResponse.fromJson(json['response'])
            : null,
        takeOverItem:
            json['data'] != null ? TakeOverItem.fromJson(json['data']) : null,
      );
}
