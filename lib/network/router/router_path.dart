import 'package:flutter_inapp_takeover/network/network.dart';

extension InAppTakeOverRouterPath on InAppTakeOverRouter {
  String get host => InAppTakeOverNetworkLayer().baseUrl;

  // ignore: missing_return
  String get path {
    switch (this) {
      case InAppTakeOverRouter.getTakeOvers:
        return '/prod/activepage';
      case InAppTakeOverRouter.markAsRead:
        return '/prod/read';
    }
  }
}
