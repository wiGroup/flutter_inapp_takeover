import 'package:flutter_abstract_network_payload/network/network.dart';

import '../network.dart';

extension InAppTakeOverRouterMethod on InAppTakeOverRouter {
  // ignore: missing_return
  HttpMethod get method {
    switch (this) {
      case InAppTakeOverRouter.getTakeOvers:
        return HttpMethod.get;
      case InAppTakeOverRouter.markAsRead:
        return HttpMethod.put;
    }
  }
}
