import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_abstract_network_payload/network/network.dart';
import 'package:flutter_inapp_takeover/constants.dart';
import 'package:flutter_inapp_takeover/models/models.dart';
import 'package:http/http.dart' as http;

import 'network.dart';

@protected
class InAppTakeOverNetworkLayer {
  String baseUrl;
  static InAppTakeOverNetworkLayer _instance;
  InAppTakeOverNetworkLayer._internal();

  factory InAppTakeOverNetworkLayer() {
    _instance ??= InAppTakeOverNetworkLayer._internal();

    return _instance;
  }

  init({String baseUrl}) {
    this.baseUrl = baseUrl;
  }

  Future<T> request<T>(HttpMethod method, InAppTakeOverRouter router,
      InAppTakeOverPayLoad payload) async {
    var url = Uri.https(baseUrl, router.path, payload.queryParameters);
    var response;

    try {
      switch (method) {
        case HttpMethod.put:
          response = await http.put(url);
          break;
        case HttpMethod.get:
          response = await http.get(url);
          break;
        case HttpMethod.post:
          break;
        case HttpMethod.delete:
          break;
      }

      if (response.statusCode == 200) {
        return json.decode(response.body);
      } else {
        return Future.error(GENERAL_ERROR_MESSAGE);
      }
    } catch (e) {
      return Future.error(GENERAL_ERROR_MESSAGE);
    }
  }
}
